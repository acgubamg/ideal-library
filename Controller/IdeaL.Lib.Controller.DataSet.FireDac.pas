unit IdeaL.Lib.Controller.DataSet.FireDac;

interface

uses
  System.Classes,
  System.SysUtils,

  Data.DB,

  IdeaL.Lib.IDataSet,

  FireDac.Stan.Intf,
  FireDac.Stan.Option,
  FireDac.Stan.Param,
  FireDac.Stan.Error,
  FireDac.DatS,
  FireDac.Phys.Intf,
  FireDac.DApt.Intf,
  FireDac.Stan.Async,
  FireDac.DApt,
  FireDac.Comp.DataSet,
  FireDac.Comp.Client;

type
  TDataSetFireDac = class(TComponent, IDataSet)
  private
    FDbConnection: TFDConnection;
    function GetDbConnection: TComponent;
    procedure SetDbConnection(const Value: TComponent);
    { Private declarations }
  public
    constructor Create(AOwner: TComponent);
    destructor Destroy; override;

    procedure ConnClose();
    function DataBasePath(): string;
    procedure QryExecSql(const ASql: string);
    procedure ConnExecSql(const ASql: string);
    function GetListagem(const ASql: string): TDataSet;
    procedure ConnCommit();
    procedure ConnRollBack();
    procedure ConnStartTransaction();

    function GetDateTime(): string;
    function GetResultJson(const ASql: string): string;

    property DbConnection: TComponent read GetDbConnection write SetDbConnection;
  end;

var
  DSFireDac: TDataSetFireDac;

implementation

{ TZeosDataSet }
procedure TDataSetFireDac.ConnClose;
begin
  FDbConnection.Connected := False;
end;

procedure TDataSetFireDac.ConnCommit;
begin
  FDbConnection.Commit;
end;

procedure TDataSetFireDac.ConnExecSql(const ASql: string);
begin
  if not(FDbConnection.Connected) then
    FDbConnection.Connected := True;
  FDbConnection.ExecSQL(ASql);
end;

procedure TDataSetFireDac.ConnRollBack;
begin
  FDbConnection.Rollback;
end;

procedure TDataSetFireDac.ConnStartTransaction;
begin
  FDbConnection.StartTransaction;
end;

constructor TDataSetFireDac.Create(AOwner: TComponent);
begin
  // inherited;
  if Assigned(AOwner) then // so ate ter corrigido tds pontos
    raise Exception.Create(Self.ClassName + ' Create is wrong');
  DSFireDac := Self;
  FDbConnection := nil;
end;

function TDataSetFireDac.DataBasePath: string;
begin
  Result := FDbConnection.Params.Values['Database'];
end;

destructor TDataSetFireDac.Destroy;
begin
  DSFireDac := nil;
  inherited;
end;

function TDataSetFireDac.GetDateTime: string;
var
  LQry: TFDQuery;
begin
  try
    LQry := TFDQuery.Create(nil);
    LQry.Connection := FDbConnection;
    if (LowerCase(FDbConnection.DriverName).Equals('fb')) then
      LQry.SQL.Add
        ('select cast(''Now'' as TIMESTAMP) as LDateTime from rdb$database')
    else if (LowerCase(FDbConnection.DriverName).Equals('mysql')) then
      LQry.SQL.Add('SELECT NOW() as LDateTime');
    LQry.Open;

    Result := FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', LQry.FieldByName('LDateTime').AsDateTime);
    LQry.Close;
  finally
    FreeAndNil(LQry);
  end;
end;

function TDataSetFireDac.GetDbConnection: TComponent;
begin
  Result := FDbConnection;
end;

function TDataSetFireDac.GetListagem(const ASql: string): TDataSet;
begin
  try
    Result := TFDQuery.Create(nil);
    TFDQuery(Result).Close;
    TFDQuery(Result).Connection := FDbConnection;
    TFDQuery(Result).SQL.Clear;
    TFDQuery(Result).SQL.Add(ASql);
  {$IFDEF MSWINDOWS}
    TFDQuery(Result).SQL.SaveToFile('ScriptGetDataSet.sql');
  {$ENDIF}
    TFDQuery(Result).Open;
  except
    FreeAndNil(Result);
    raise ;
  end;
end;

function TDataSetFireDac.GetResultJson(const ASql: string): string;
var
  LDataSet: TDataSet;
begin
  try
    LDataSet := nil;
    Result := EmptyStr;
    LDataSet := GetListagem(ASql);

    Result := '[{' + '"Result":"OK"' + ',"Values":[' + LDataSet.FieldByName('Json').AsString + ']' + '}]';
  finally
    FreeAndNil(LDataSet);
  end;
end;

procedure TDataSetFireDac.QryExecSql(const ASql: string);
var
  LQry: TFDQuery;
begin
  try
    LQry := TFDQuery.Create(nil);
    LQry.Connection := FDbConnection;
    LQry.Close;
    LQry.SQL.Add(ASql);

{$IFDEF MSWINDOWS}
    LQry.SQL.SaveToFile('ScriptQryExecSql.sql');
{$ENDIF}
    LQry.ExecSQL;
    LQry.Close;
  finally
    FreeAndNil(LQry);
  end;
end;

procedure TDataSetFireDac.SetDbConnection(const Value: TComponent);
begin
  FDbConnection := TFDConnection(Value);
end;

end.
