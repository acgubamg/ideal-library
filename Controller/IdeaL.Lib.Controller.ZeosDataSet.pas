unit IdeaL.Lib.Controller.ZeosDataSet;

interface

uses
  System.Classes,
  System.SysUtils,

  Data.DB,

  IdeaL.Lib.IDataSet,

  ZAbstractRODataset,
  ZAbstractDataset,
  ZDataset,
  ZAbstractConnection,
  ZConnection;

type
  TDMZeosDataSet = class(TComponent, IDataSet)
  private
    FDbConnection: TZConnection;

    procedure SetDbConnection(Value: TComponent);
    function GetDbConnection: TComponent;
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); // override;
    destructor Destroy; override;

    property Connection: TZConnection read FDbConnection;
    procedure ConnClose();
    function DataBasePath(): string;
    procedure QryExecSql(const ASql: string);
    procedure ConnExecSql(const ASql: string);
    function GetListagem(const ASql: string): TDataSet;
    procedure ConnCommit();
    procedure ConnRollBack();
    procedure ConnStartTransaction();

    function GetDateTime(): String;
    function GetResultJson(const ASql: string): string;


    property DbConnection: TComponent read GetDbConnection write SetDbConnection;
  end;

var
  DMZeosDataSet: TDMZeosDataSet;

implementation

uses
  IdeaL.Lib.Controller.ZeosConnection;

{ TZeosDataSet }

procedure TDMZeosDataSet.ConnClose;
begin
  FDbConnection.Connected := False;
end;

procedure TDMZeosDataSet.ConnCommit;
begin
  FDbConnection.Commit;
end;

procedure TDMZeosDataSet.ConnExecSql(const ASql: string);
begin
  if not(FDbConnection.Connected) then
    FDbConnection.Connected := True;
  FDbConnection.ExecuteDirect(ASql);
end;

procedure TDMZeosDataSet.ConnRollBack;
begin
  FDbConnection.Rollback;
end;

procedure TDMZeosDataSet.ConnStartTransaction;
begin
  if not FDbConnection.Connected then
    FDbConnection.Connected := True;
  FDbConnection.StartTransaction;
end;

constructor TDMZeosDataSet.Create(AOwner: TComponent);
begin
  //inherited;
  if Assigned(AOwner) then // so ate ter corrigido tds pontos
    raise Exception.Create(Self.ClassName + ' Create is wrong');
  DMZeosDataSet := Self;
  FDbConnection := nil;
end;

function TDMZeosDataSet.DataBasePath: string;
begin
  Result := FDbConnection.Database;
end;

destructor TDMZeosDataSet.Destroy;
begin
  // DMZeosDataSet := nil;
  FDbConnection := nil;
  inherited;
end;

function TDMZeosDataSet.GetDateTime(): string;
var
  LQry: TZQuery;
begin
  Result := EmptyStr;
  try
    LQry := TZQuery.Create(nil);
    LQry.Connection := FDbConnection;
    if (LowerCase(FDbConnection.Protocol).Equals('firebird')) then
      LQry.SQL.Add
        ('select cast(''Now'' as TIMESTAMP) as LDateTime from rdb$database')
    else if (LowerCase(FDbConnection.Protocol).Equals('mysql')) then
      LQry.SQL.Add('SELECT NOW() as LDateTime');
    LQry.Open;

    Result := FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', LQry.FieldByName('LDateTime').AsDateTime);
    LQry.Close;
  finally
    FreeAndNil(LQry);
  end;
end;

function TDMZeosDataSet.GetDbConnection: TComponent;
begin
  Result := TComponent(FDbConnection);
end;

function TDMZeosDataSet.GetListagem(const ASql: string): TDataSet;
begin
  try
    Result := TZQuery.Create(nil);
    TZQuery(Result).Close;
    TZQuery(Result).Connection := FDbConnection;
    TZQuery(Result).SQL.Clear;
    TZQuery(Result).SQL.Add(ASql);
  {$IFDEF DEBUG}
    TZQuery(Result).SQL.SaveToFile('ScriptGetDataSet.sql');
  {$ENDIF}
    TZQuery(Result).Open;
  except
    FreeAndNil(Result);
    raise ;
  end;
end;

function TDMZeosDataSet.GetResultJson(const ASql: string): string;
var
  LDataSet: TDataSet;
begin
  try
    LDataSet := nil;
    Result := EmptyStr;
    LDataSet := GetListagem(ASql);

    Result := '[{' + '"Result":"OK"' + ',"Values":[' +
      LDataSet.FieldByName('Json').AsString + ']' + '}]';
  finally
    FreeAndNil(LDataSet);
  end;
end;

procedure TDMZeosDataSet.QryExecSql(const ASql: string);
var
  LQry: TZQuery;
begin
  try
    LQry := TZQuery.Create(nil);
    LQry.Connection := FDbConnection;
    LQry.Close;
    LQry.SQL.Add(ASql);

{$IFDEF DEBUG}
    LQry.SQL.SaveToFile('ScriptQryExecSql.sql');
{$ENDIF}
    LQry.ExecSQL;
    LQry.Close;
  finally
    FreeAndNil(LQry);
  end;
end;

procedure TDMZeosDataSet.SetDbConnection(Value: TComponent);
begin
  FDbConnection := TZConnection(Value);
end;

end.
