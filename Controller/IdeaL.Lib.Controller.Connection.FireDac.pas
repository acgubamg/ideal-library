unit IdeaL.Lib.Controller.Connection.FireDac;

interface

uses
  System.SysUtils,

{$IFDEF MSWINDOWS}
  FireDac.VCLUI.Wait,
{$ENDIF}
  FireDac.Stan.Intf,
  FireDac.Stan.Option,
  FireDac.Stan.Error,
  FireDac.UI.Intf,
  FireDac.Phys.Intf,
  FireDac.Stan.Def,
  FireDac.Stan.Pool,
  FireDac.Stan.Async,
  FireDac.Phys,
  FireDac.FMXUI.Wait,
  FireDac.Comp.Client,
  FireDac.Phys.SQLite,
  FireDac.Phys.SQLiteDef,
  FireDac.Stan.ExprFuncs,
  FireDAC.Comp.UI,
  FireDAC.Phys.SQLiteWrapper.Stat

{$IFDEF MSWINDOWS}
  ,
// Firebird
  FireDAC.Phys.FBDef,
  FireDAC.Phys.IBBase,
  FireDAC.Phys.FB,
// Firebird

// MySQL
  FireDAC.Phys.MySQLDef,
  FireDAC.Phys.MySQL
// MySQL
{$ENDIF}
  ;

type
  TFireDacConn = class
  private
    FFDConn: TFDConnection;
    FFDGUIxWaitCursor: TFDGUIxWaitCursor;
    FFDPhysDriverLink: TFDPhysDriverLink;
    procedure SetFDConn(const Value: TFDConnection);
    { private declarations }
  protected
    { protected declarations }
  public
    constructor Create();
    destructor Destroy; override;

    property FDConn: TFDConnection read FFDConn write SetFDConn;

    procedure SetConfiguration(const ADataBase: string);
    procedure SetConfigurationSqlite(const ADataBase: string);
{$IFDEF MSWINDOWS}
    procedure SetConfigurationFirebird(const AHost, APort, ADataBase, AUserName, APassword: string);
    procedure SetConfigurationMySQL(const AHost, APort, ADataBase, AUserName, APassword: string);
{$ENDIF}
    { public declarations }
  published
    { published declarations }
  end;

implementation

{ TFireDacConn }

constructor TFireDacConn.Create();
begin
  FFDConn := TFDConnection.Create(nil);
  FFDConn.LoginPrompt := False;
  FFDGUIxWaitCursor := TFDGUIxWaitCursor.Create(nil);
  FFDPhysDriverLink := nil;
end;

destructor TFireDacConn.Destroy;
begin
  FFDConn.Close;
  FreeAndNil(FFDConn);
  FreeAndNil(FFDGUIxWaitCursor);
  FreeAndNil(FFDPhysDriverLink);
  inherited;
end;

procedure TFireDacConn.SetConfiguration(const ADataBase: string);
begin
  FFDConn.Close;
  FFDConn.Params.Values['Database'] := ADataBase;
  FFDConn.Params.Values['OpenMode'] := 'ReadWrite';
end;

{$IFDEF MSWINDOWS}
procedure TFireDacConn.SetConfigurationFirebird(const AHost, APort, ADataBase, AUserName, APassword: string);
begin
  FFDConn.Close;
  FreeAndNil(FFDPhysDriverLink);
  FFDPhysDriverLink := TFDPhysFBDriverLink.Create(nil);
  FFDConn.DriverName := '';
  FFDConn.DriverName := 'FB';
  if AHost.Trim.IsEmpty then
    FFDConn.Params.Values['Protocol'] := 'Local'
  else
    FFDConn.Params.Values['Protocol'] := 'TCPIP';
  FFDConn.Params.Values['Server'] := AHost;
  FFDConn.Params.Values['Port'] := APort;
  FFDConn.Params.Values['User_Name'] := AUserName;
  FFDConn.Params.Values['Password'] := APassword;
  SetConfiguration(ADataBase);
end;

procedure TFireDacConn.SetConfigurationMySQL(const AHost, APort, ADataBase,
  AUserName, APassword: string);
begin
  FFDConn.Close;
  FreeAndNil(FFDPhysDriverLink);
  FFDPhysDriverLink := TFDPhysMySQLDriverLink.Create(nil);
  FFDConn.DriverName := '';
  FFDConn.DriverName := 'MySQL';
  FFDConn.Params.Values['Server'] := AHost;
  FFDConn.Params.Values['Port'] := APort;
  FFDConn.Params.Values['User_Name'] := AUserName;
  FFDConn.Params.Values['Password'] := APassword;
  SetConfiguration(ADataBase);
end;
{$ENDIF}

procedure TFireDacConn.SetConfigurationSqlite(const ADataBase: string);
begin
  FFDConn.Close;
  FreeAndNil(FFDPhysDriverLink);
  FFDPhysDriverLink := TFDPhysSQLiteDriverLink.Create(nil);
  FFDConn.DriverName := 'SQLite';
  SetConfiguration(ADataBase);
end;

procedure TFireDacConn.SetFDConn(const Value: TFDConnection);
begin
  FFDConn := Value;
end;

end.

