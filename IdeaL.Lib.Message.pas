﻿unit IdeaL.Lib.Message;

interface

uses
  System.Classes,
  System.UITypes,
  System.UIConsts,
  System.SysUtils,
  System.Types,
  System.Generics.Collections,

  Fmx.Controls,
  Fmx.Objects,
  Fmx.Graphics,
  Fmx.Layouts,
  Fmx.Types,
  Fmx.StdCtrls,
  Fmx.Effects,
  Fmx.Dialogs,
  Fmx.Forms,
  Fmx.Ani;

type
  TMsgType = (mtInformation, mtQuestion, mtError);
  TMsgTypeButton = (mtbOk, mtbYesNo, mtbLoading, mtbNone);

  TFmxMessageItem = class;

  TFmxMessageBase = class
  private
    FLytBackground: TLayout;
    FRctBackground: TRectangle;
    FObjLst: TObjectList<TFmxMessageItem>;

    FParent: TControl;

    FOnHide: TProc;
    FIsDestroyObjects: Boolean;

    procedure RctBackgroundClick(Sender: TObject);
    procedure RctBackgroundTap(Sender: TObject; const Point: TPointF);

    procedure ItemOnHide(Sender: TObject);
    { private declarations }
  protected
    procedure Hide; virtual;
    procedure Show; virtual;

    procedure PrepareBackground; virtual;
    // procedure Resize; virtual; abstract;
    procedure DoResize(Sender: TObject);

    procedure AddDialog(
      AMsg: string;
      ABackgroundColor: string;
      ATitle: string;
      ATextColor: string;
      AFontName: string;
      ABtnStyleLookup: string;
      AMsgTypeButton: TMsgTypeButton = mtbOk;
      AMsgType: TMsgType = mtInformation;
      ACloseDialogProc: TInputCloseDialogProc = nil;
      ALenght: Single = -1
      );
    procedure AddToast(
      AMessage: string;
      ABackgroundColor: string;
      ATextColor: string;
      AFontName: string;
      ADelay: Single = 2;
      AProcClick: TNotifyEvent = nil;
      AProcFloatAnimationFinish: TNotifyEvent = nil); virtual;
    { protected declarations }
  public
    constructor Create(AParent: TControl); virtual;
    destructor Destroy; override;

    function Count: Integer;

    property IsDestroyObjects: Boolean read FIsDestroyObjects write FIsDestroyObjects;
    property RctBackground: TRectangle read FRctBackground;
    property OnHide: TProc read FOnHide write FOnHide;
    { public declarations }
  published
    { published declarations }
  end;

  TFmxMessageItem = class
  private
    FName: string;
    FLytBackground: TLayout;
    FRctBackground: TRectangle;
    FVertScrl: TVertScrollBox;
    FLblMessage: TText;
    FExtraHeight: Single;

    FParent: TControl;
    FMessage: string;
    FBackgroundColor: string;
    FTextColor: string;
    FFontName: string;
    FOnHide: TNotifyEvent;
    FIsFFloatAnimationHeightFinish: Boolean;

    { private declarations }
  protected
    property Name: string read FName;
    property LytBackground: TLayout read FLytBackground;
    property RctBackground: TRectangle read FRctBackground;
    property LblMessage: TText read FLblMessage;
    property ExtraHeight: Single read FExtraHeight write FExtraHeight;

    procedure PrepareBackground; virtual;
    function GetHeight: Single;
    procedure Resize; virtual;
    { protected declarations }
  public
    constructor Create(AParent: TControl; AMessage, ABackgroundColor, ATextColor, AFontName: string); virtual;
    destructor Destroy; override;

    procedure Hide; virtual;
    procedure Show; virtual;

    property OnHide: TNotifyEvent read FOnHide write FOnHide;
    { public declarations }
  published
    { published declarations }
  end;

  TFmxMessageToast = class(TFmxMessageItem)
  private
    FFloatAnimation: TFloatAnimation;
    FDelay: Single; // in seconds
    FOnFloatAnimationFinish: TNotifyEvent;
    FOnClick: TNotifyEvent;

    { private declarations }
  protected
    procedure Hide; override;
    procedure Show; override;
    procedure PrepareBackground; override;
    procedure Resize; override;

    procedure FloatAnimationFinish(Sender: TObject);
    procedure Click(Sender: TObject);
    procedure Tap(Sender: TObject; const Point: TPointF);
    { protected declarations }
  public
    constructor Create(
      AParent: TControl;
      AMessage: string;
      ABackgroundColor: string;
      ATextColor: string;
      AFontName: string;
      ADelay: Single = 2
      ); reintroduce;
    destructor Destroy; override;

    property OnFloatAnimationFinish: TNotifyEvent read FOnFloatAnimationFinish write FOnFloatAnimationFinish;
    property OnClick: TNotifyEvent read FOnClick write FOnClick;
    { public declarations }
  published
    { published declarations }
  end;

  TFmxMessageDialog = class(TFmxMessageItem)
  private
    FBtnStyleLookup: string;
    FTitle: string;
    FLblTitle: TText;
    FMsgType: TMsgType;
    FCloseDialogProc: TInputCloseDialogProc;
    FMsgTypeButton: TMsgTypeButton;
    FLenght: Single;

    FLytButton: TLayout;
    { private declarations }
  protected
    procedure Hide; override;
    procedure Show; override;
    procedure PrepareBackground; override;
    procedure Resize; override;

    procedure Click(Sender: TObject);
    procedure Tap(Sender: TObject; const Point: TPointF);
    { protected declarations }
  public
    constructor Create(
      AParent: TControl;
      AMessage: string;
      ABackgroundColor: string;
      ATextColor: string;
      AFontName: string;
      ABtnStyleLookup: string;
      ATitle: String;
      ALenght: Single = -1
      ); reintroduce;
    destructor Destroy; override;

    class procedure OnInitialization;
    class procedure OnFinalization;

    class var
      OkText: string;
      YesText: string;
      NoText: string;

    property MsgTypeButton: TMsgTypeButton read FMsgTypeButton write FMsgTypeButton;
    property MsgType: TMsgType read FMsgType write FMsgType;
    property CloseDialogProc: TInputCloseDialogProc read FCloseDialogProc write FCloseDialogProc;

    { public declarations }
  published
    { published declarations }
  end;

  TFmxMessage = class
  private
  class var
    FDialogBackground: TFmxMessageBase;
    FToastBackground: TFmxMessageBase;

    { private declarations }
  protected
    class procedure DoOnHideToast;
    class procedure DoOnHideDialog;
    { protected declarations }
  public
    class procedure OnInitialization;
    class procedure OnFinalization;

    class procedure Dialog(
      AMsg: string;
      ATitle: string;
      ATextColor: string;
      AFontName: string;
      ABackgroundColor: string;
      ABtnStyleLookup: string;
      AMsgTypeButton: TMsgTypeButton = mtbOk;
      AMsgType: TMsgType = mtInformation;
      ACloseDialogProc: TInputCloseDialogProc = nil;
      AParent: TControl = nil;
      ALenght: Single = -1
      );

    class procedure Toast(
      AMsg, AFontName: string;
      const ADelay: Single; // In seconds
      AProcClick: TNotifyEvent = nil;
      AProcFloatAnimationFinish: TNotifyEvent = nil;
      AParent: TControl = nil
      ); overload;
    class procedure Toast(
      AMsg, AFontName: string;
      AProcClick: TNotifyEvent = nil;
      AProcFloatAnimationFinish: TNotifyEvent = nil;
      AParent: TControl = nil
      ); overload;
    { public declarations }
  published
    { published declarations }
  end;

implementation

{ TFmxMessageBase }

procedure TFmxMessageBase.AddDialog(AMsg, ABackgroundColor, ATitle, ATextColor, AFontName,
  ABtnStyleLookup: string; AMsgTypeButton: TMsgTypeButton; AMsgType: TMsgType;
  ACloseDialogProc: TInputCloseDialogProc; ALenght: Single);
var
  LItem: TFmxMessageDialog;
begin
  LItem := TFmxMessageDialog.Create(
    FLytBackground,
    AMsg,
    ABackgroundColor,
    ATextColor,
    AFontName,
    ABtnStyleLookup,
    ATitle,
    ALenght
    );
  LItem.OnHide := ItemOnHide;
  LItem.MsgTypeButton := AMsgTypeButton;
  LItem.MsgType := AMsgType;
  LItem.CloseDialogProc := ACloseDialogProc;
  { LItem.OnClick := AProcClick;
    LItem.OnFloatAnimationFinish := AProcFloatAnimationFinish; }
  if not Assigned(FObjLst) then
    FObjLst := TObjectList<TFmxMessageItem>.Create;
  FObjLst.Add(LItem);
  LItem.PrepareBackground;
  LItem.Show;
end;

procedure TFmxMessageBase.AddToast(AMessage, ABackgroundColor, ATextColor, AFontName: string; ADelay: Single;
  AProcClick, AProcFloatAnimationFinish: TNotifyEvent);
var
  LItem: TFmxMessageToast;
begin
  LItem := TFmxMessageToast.Create(FLytBackground, AMessage, ABackgroundColor, ATextColor, AFontName, ADelay);
  LItem.OnHide := ItemOnHide;
  LItem.OnClick := AProcClick;
  LItem.OnFloatAnimationFinish := AProcFloatAnimationFinish;
  if not Assigned(FObjLst) then
    FObjLst := TObjectList<TFmxMessageItem>.Create;
  FObjLst.Add(LItem);
  LItem.PrepareBackground;
  LItem.Show;
end;

function TFmxMessageBase.Count: Integer;
begin
  Result := 0;
  if Assigned(FObjLst) then
    Result := FObjLst.Count;
end;

constructor TFmxMessageBase.Create(AParent: TControl);
begin
  FObjLst := TObjectList<TFmxMessageItem>.Create;

  FParent := AParent;
  FRctBackground := nil;
  FLytBackground := nil;
  FIsDestroyObjects := False;
end;

destructor TFmxMessageBase.Destroy;
begin
  if Assigned(FLytBackground) then
  begin
    FLytBackground.OnResize := nil;
  end;

  Hide;
  inherited;
end;

procedure TFmxMessageBase.DoResize(Sender: TObject);
begin
  FLytBackground.OnResize := nil;
  try
    if Assigned(FObjLst) then
    begin
      for var LItem in FObjLst do
      begin
        if Assigned(LItem) and not LItem.Name.Trim.IsEmpty then
          LItem.Resize;
      end;
    end;
  finally
    FLytBackground.OnResize := DoResize;
  end;
end;

procedure TFmxMessageBase.Hide;
var
  LName: string;
begin
  if Assigned(FObjLst) then
  begin
    FObjLst.Clear;
    FreeAndNil(FObjLst);
  end;

  if IsDestroyObjects then
  begin
    if Assigned(FRctBackground) and (FRctBackground.Name <> EmptyStr) then
    begin
      FRctBackground.Parent := nil;
      FreeAndNil(FRctBackground);
    end;

    if Assigned(FLytBackground) and (FLytBackground.Name <> EmptyStr) then
    begin
      FLytBackground.Parent := nil;
      FreeAndNil(FLytBackground);
    end;
  end;

  if Assigned(FOnHide) then
    FOnHide;
end;

procedure TFmxMessageBase.PrepareBackground;
var
  LRandomStr: string;
begin
  LRandomStr := Random(100000).ToString;
  if not(Assigned(FLytBackground)) then
    FLytBackground := TLayout.Create(FParent);
  FLytBackground.Visible := False;
  FLytBackground.Align := TAlignLayout.Contents;
  FLytBackground.Parent := FParent;
  // generating its name to fix a Destroy issue
  FLytBackground.Name := FLytBackground.ClassName + LRandomStr;
  FLytBackground.OnResize := DoResize;

{$REGION 'FRctBackground'}
  if not(Assigned(FRctBackground)) then
    FRctBackground := TRectangle.Create(FLytBackground);
  FRctBackground.Stroke.Kind := TBrushKind.None;
  FRctBackground.Fill.Color := StringToAlphaColor('Black');
  FRctBackground.Align := TAlignLayout.Contents;
  FRctBackground.Parent := FLytBackground;
  FRctBackground.Name := FRctBackground.ClassName + LRandomStr;

{$IFDEF MSWINDOWS}
  FRctBackground.OnClick := RctBackgroundClick;
{$ELSE}
  FRctBackground.OnTap := RctBackgroundTap;
{$ENDIF}
{$ENDREGION}
end;

procedure TFmxMessageBase.RctBackgroundClick(Sender: TObject);
begin
  IsDestroyObjects := True;
  Hide;
end;

procedure TFmxMessageBase.RctBackgroundTap(Sender: TObject; const Point: TPointF);
begin
  RctBackgroundClick(Sender);
end;

procedure TFmxMessageBase.Show;
begin
  FLytBackground.Visible := True;
  FLytBackground.Opacity := 0;
  FRctBackground.Opacity := 0;
  FRctBackground.Visible := True;

  FRctBackground.Opacity := 0.5;
  FLytBackground.Opacity := 1;
  FLytBackground.BringToFront;
  FLytBackground.SetFocus;
end;

procedure TFmxMessageBase.ItemOnHide(Sender: TObject);
var
  LItem: TFmxMessageItem;
begin
  if not(Assigned(FObjLst)) then
    Exit;

  LItem := FObjLst.Extract(TFmxMessageItem(Sender));
  if Assigned(LItem) { and not LItem.Name.Trim.IsEmpty } then
  begin
    LItem.OnHide := nil;
    FreeAndNil(LItem);
  end;

  if Assigned(FOnHide) then
    FOnHide;
end;

{ TFmxMessage }

class procedure TFmxMessage.Dialog(AMsg, ATitle, ATextColor, AFontName,
  ABackgroundColor, ABtnStyleLookup: string; AMsgTypeButton: TMsgTypeButton; AMsgType: TMsgType;
  ACloseDialogProc: TInputCloseDialogProc; AParent: TControl; ALenght: Single);
begin
  if not Assigned(AParent) then
    AParent := TControl(Application.MainForm);

  if not(Assigned(FDialogBackground)) then
    FDialogBackground := TFmxMessageBase.Create(AParent);

  FDialogBackground.OnHide := DoOnHideDialog;

  FDialogBackground.PrepareBackground;

  FDialogBackground.AddDialog(
    AMsg,
    ABackgroundColor,
    ATitle,
    ATextColor,
    AFontName,
    ABtnStyleLookup,
    AMsgTypeButton,
    AMsgType,
    ACloseDialogProc,
    ALenght
    );
  FDialogBackground.Show;
end;

class procedure TFmxMessage.DoOnHideDialog;
begin
  if Assigned(FDialogBackground) then
  begin
    FDialogBackground.IsDestroyObjects := True;
    FreeAndNil(FDialogBackground);
  end;
end;

class procedure TFmxMessage.DoOnHideToast;
begin
  if Assigned(FToastBackground) and (FToastBackground.Count = 0) then
  begin
    FToastBackground.IsDestroyObjects := True;
    FreeAndNil(FToastBackground);
  end;
end;

class procedure TFmxMessage.OnFinalization;
begin
  if Assigned(FDialogBackground) then
  begin
    FreeAndNil(FDialogBackground);
  end;

  if Assigned(FToastBackground) then
  begin
    FreeAndNil(FToastBackground);
  end;
end;

class procedure TFmxMessage.OnInitialization;
begin
  FDialogBackground := nil;
  FToastBackground := nil;
end;

class procedure TFmxMessage.Toast(AMsg, AFontName: string; AProcClick, AProcFloatAnimationFinish: TNotifyEvent; AParent: TControl);
begin
  TFmxMessage.Toast(AMsg, AFontName, 2, AProcClick, AProcFloatAnimationFinish);
end;

class procedure TFmxMessage.Toast(AMsg, AFontName: string; const ADelay: Single; AProcClick, AProcFloatAnimationFinish: TNotifyEvent;
  AParent: TControl);
begin
  if not Assigned(AParent) then
    AParent := TControl(Application.MainForm);

  if not(Assigned(FToastBackground)) then
    FToastBackground := TFmxMessageBase.Create(AParent);

  FToastBackground.OnHide := DoOnHideToast;

  FToastBackground.PrepareBackground;

  FToastBackground.AddToast(AMsg, 'Black', 'White', AFontName, ADelay, AProcClick, AProcFloatAnimationFinish);
  FToastBackground.Show;
  FToastBackground.RctBackground.Visible := False;
end;

{ TFmxMessageItem }

constructor TFmxMessageItem.Create(AParent: TControl; AMessage, ABackgroundColor, ATextColor, AFontName: string);
begin
  FLytBackground := nil;
  FRctBackground := nil;
  FVertScrl := nil;
  FLblMessage := nil;
  FOnHide := nil;
  FExtraHeight := 0;
  FName := Random(100000).ToString;

  FParent := AParent;
  FMessage := AMessage;
  FBackgroundColor := ABackgroundColor;
  FTextColor := ATextColor;
  FFontName := AFontName;

  FRctBackground := nil;
  FIsFFloatAnimationHeightFinish := True;
end;

destructor TFmxMessageItem.Destroy;
begin
  FName := EmptyStr;
  inherited;
end;

function TFmxMessageItem.GetHeight: Single;
const
  LMinHeight = 40;
var
  LParentHeight: Single;
begin
  FLblMessage.RecalcSize;

  Result := ExtraHeight +
    FVertScrl.Margins.Bottom +
    FVertScrl.Margins.Top +
    FLblMessage.Height;

  // FLytBackground.Height := Result;

  LParentHeight := 0;

  if (FParent.Parent is TForm) then
    LParentHeight := TForm(FParent.Parent).ClientHeight
  else
    LParentHeight := TControl(FParent.Parent).Height;

  if (Result > (LParentHeight - LMinHeight)) then
    Result := LParentHeight - LMinHeight;

  if (Result < LMinHeight) then
    Result := LMinHeight;
end;

procedure TFmxMessageItem.Hide;
begin
  if Assigned(FLblMessage) and (FLblMessage.Name <> EmptyStr) then
  begin
    FreeAndNil(FLblMessage);
  end;

  if Assigned(FVertScrl) and (FVertScrl.Name <> EmptyStr) then
  begin
    FreeAndNil(FVertScrl);
  end;

  if Assigned(FRctBackground) and (FRctBackground.Name <> EmptyStr) then
  begin
    FreeAndNil(FRctBackground);
  end;

  if Assigned(FLytBackground) and (FLytBackground.Name <> EmptyStr) then
  begin
    FreeAndNil(FLytBackground);
  end;

  if Assigned(FOnHide) then
    FOnHide(Self);
end;

procedure TFmxMessageItem.PrepareBackground;
begin
  if not Assigned(FLytBackground) then
    FLytBackground := TLayout.Create(FParent);
  FLytBackground.Parent := FParent;
  FLytBackground.Align := TAlignLayout.VertCenter;
  FLytBackground.Visible := True;
  FLytBackground.Name := 'LytBackground' + Name;
  FLytBackground.Height := 40;
  FLytBackground.Width := 50;
  FLytBackground.Margins.Left := 40;
  FLytBackground.Margins.Right := 40;
  FLytBackground.Opacity := 0;

  if not Assigned(FRctBackground) then
    FRctBackground := TRectangle.Create(LytBackground);
  FRctBackground.Name := 'RctBackground' + Name;
  FRctBackground.Parent := LytBackground;
  FRctBackground.Align := TAlignLayout.Contents;
  FRctBackground.XRadius := 5;
  FRctBackground.YRadius := 5;
  FRctBackground.Stroke.Color := StringToAlphaColor(FBackgroundColor);
  FRctBackground.Fill.Color := StringToAlphaColor(FBackgroundColor);
  FRctBackground.HitTest := True;

  if not Assigned(FVertScrl) then
    FVertScrl := TVertScrollBox.Create(LytBackground);
  FVertScrl.Name := 'VertScrl' + Name;
  FVertScrl.Parent := LytBackground;
  FVertScrl.Align := TAlignLayout.Client;
  FVertScrl.Margins.Bottom := 10;
  FVertScrl.Margins.Left := 10;
  FVertScrl.Margins.Right := 10;
  FVertScrl.Margins.Top := 10;

  if not Assigned(FLblMessage) then
    FLblMessage := TText.Create(FVertScrl);
  FLblMessage.Name := 'LblMessage' + Name;
  FLblMessage.Parent := FVertScrl;
  FLblMessage.Align := TAlignLayout.Top;
  FLblMessage.AutoSize := True;
  FLblMessage.TextSettings.Font.Family := FFontName;
  FLblMessage.TextSettings.Font.Size := 16;
  FLblMessage.TextSettings.FontColor := StringToAlphaColor(FTextColor);
  FLblMessage.TextSettings.HorzAlign := TTextAlign.Center;
  FLblMessage.TextSettings.VertAlign := TTextAlign.Center;
  FLblMessage.TextSettings.WordWrap := True;

  { var
    LRect := TRectangle.Create(FLblMessage);
    LRect.Parent := FLblMessage;
    LRect.Align := TAlignLayout.Contents; }
end;

procedure TFmxMessageItem.Resize;
begin
  FLytBackground.Height := GetHeight;
end;

procedure TFmxMessageItem.Show;
begin
  FLytBackground.Visible := True;
  FLytBackground.BringToFront;

  FLblMessage.Text := EmptyStr;

  TThread.CreateAnonymousThread(
    procedure
    begin
      TThread.Sleep(50);
      TThread.Synchronize(nil,
        procedure
        begin
          FLblMessage.Text := FMessage;
          FLblMessage.BringToFront;
          Resize;
          FLytBackground.Opacity := 1;
        end);
    end).Start;
end;

{ TFmxMessageToast }

constructor TFmxMessageToast.Create(AParent: TControl; AMessage, ABackgroundColor, ATextColor, AFontName: string; ADelay: Single);
begin
  inherited Create(AParent, AMessage, ABackgroundColor, ATextColor, AFontName);
  FDelay := ADelay;
  if FDelay < 2 then
    FDelay := 2;
  FFloatAnimation := nil;
  FOnFloatAnimationFinish := nil;
  FOnClick := nil;
end;

destructor TFmxMessageToast.Destroy;
begin
  FOnClick := nil;
  FOnFloatAnimationFinish := nil;
  inherited;
end;

procedure TFmxMessageToast.FloatAnimationFinish(Sender: TObject);
begin
  if Assigned(FOnFloatAnimationFinish) then
    FOnFloatAnimationFinish(Sender);

  TThread.CreateAnonymousThread(
    procedure
    begin
      TThread.Synchronize(nil,
        procedure
        begin
          Hide;
        end);
    end).Start;
end;

procedure TFmxMessageToast.Hide;
begin
  if Assigned(FFloatAnimation) and (FFloatAnimation.Name <> EmptyStr) then
  begin
    FOnFloatAnimationFinish := nil;
    FFloatAnimation.Stop;
    FFloatAnimation.Enabled := False;
    FFloatAnimation.Parent := nil;
    FreeAndNil(FFloatAnimation);
  end;
  inherited;
end;

procedure TFmxMessageToast.PrepareBackground;
begin
  inherited;
  LytBackground.Align := TAlignLayout.Bottom;
  LytBackground.Margins.Bottom := 10;
  RctBackground.Opacity := 0.7;

  if not Assigned(FFloatAnimation) then
    FFloatAnimation := TFloatAnimation.Create(FLytBackground);
  FFloatAnimation.Name := 'FloatAnimation' + Name;
  FFloatAnimation.Delay := FDelay;
  FFloatAnimation.Duration := 0.2;
  FFloatAnimation.PropertyName := 'Opacity';
  FFloatAnimation.StartValue := 1;
  FFloatAnimation.StopValue := 0;
  FFloatAnimation.OnFinish := FloatAnimationFinish;
  FFloatAnimation.Parent := LytBackground;
end;

procedure TFmxMessageToast.Resize;
begin
  inherited;

end;

procedure TFmxMessageToast.Show;
begin
  inherited;

  { TThread.CreateAnonymousThread(
    procedure
    begin
    TThread.Sleep(50);
    TThread.Synchronize(nil,
    procedure
    begin
    LblMessage.BringToFront;
    Resize;
    end);
    end).Start; }

  FFloatAnimation.Enabled := True;
  LblMessage.TextSettings.HorzAlign := TTextAlign.Center;
end;

procedure TFmxMessageToast.Click(Sender: TObject);
begin
  if Assigned(OnClick) then
    OnClick(Self);
end;

procedure TFmxMessageToast.Tap(Sender: TObject; const Point: TPointF);
begin
  Click(Sender);
end;

{ TFmxMessageDialog }

procedure TFmxMessageDialog.Click(Sender: TObject);
var
  LModalResult: Integer;
  LCloseDialogProc: TInputCloseDialogProc;
begin
  LModalResult := TButton(Sender).Tag;

  if (Assigned(FCloseDialogProc)) then
  begin
    LCloseDialogProc := FCloseDialogProc;
    (* {$IFDEF MSWINDOWS}
      LCloseDialogProc(LModalResult);
      {$ELSE} *)
    TThread.CreateAnonymousThread(
      procedure
      begin

        TThread.Sleep(50);
        TThread.Synchronize(TThread.CurrentThread,
          procedure
          begin
            LCloseDialogProc(LModalResult);
            Screen.ActiveForm := Application.MainForm;
          end);
      end).Start;
    // {$ENDIF}
  end;

  Hide;
end;

constructor TFmxMessageDialog.Create(AParent: TControl; AMessage,
  ABackgroundColor, ATextColor, AFontName, ABtnStyleLookup, ATitle: String;
ALenght: Single);
begin
  inherited Create(AParent, AMessage, ABackgroundColor, ATextColor, AFontName);

  FBtnStyleLookup := ABtnStyleLookup;
  FTitle := ATitle;
  FLblTitle := nil;
  FLytButton := nil;
  MsgTypeButton := mtbOk;
  MsgType := mtInformation;
  CloseDialogProc := nil;
  FLenght := ALenght;
end;

destructor TFmxMessageDialog.Destroy;
begin

  inherited;
end;

procedure TFmxMessageDialog.Hide;
begin

  inherited;

end;

class procedure TFmxMessageDialog.OnFinalization;
begin

end;

class procedure TFmxMessageDialog.OnInitialization;
begin
  OkText := 'OK';
  YesText := 'Yes';
  NoText := 'No';
end;

procedure TFmxMessageDialog.PrepareBackground;

  procedure ConfigButton(
    AText: String;
  AAlign: TAlignLayout;
  AModalResult: TModalResult);
  var
    LBtn: TButton;
    LStyle: TComponent;
    LStyleName: string;
    i: Integer;
  begin
    LBtn := TButton.Create(FLytButton);
    LBtn.Parent := FLytButton;
    LBtn.StyleLookup := FBtnStyleLookup;
    LBtn.Align := AAlign;
    LBtn.Text := AText;
    LBtn.Width := FLytButton.Height;
    LBtn.StyledSettings := [];
    LBtn.Font.Style := [TFontStyle.fsBold];
    LBtn.TextSettings.Font.Family := FFontName;
    LBtn.TextSettings.Font.Size := 16;
    LBtn.TextSettings.FontColor := StringToAlphaColor('White');
    LBtn.CanFocus := False;
    LBtn.Tag := AModalResult;
{$IFDEF MSWINDOWS}
    LBtn.OnClick := Click;
{$ELSE}
    LBtn.OnTap := Tap;
{$ENDIF}
  end;

begin
  inherited;
  LytBackground.Padding.Bottom := 5;
  LytBackground.Padding.Left := 5;
  LytBackground.Padding.Right := 5;
  LytBackground.Padding.Top := 5;

  if not(Assigned(FLblTitle)) then
    FLblTitle := TText.Create(LytBackground);
  FLblTitle.Name := 'LblTitle' + Name;
  FLblTitle.Parent := LytBackground;
  FLblTitle.Align := TAlignLayout.MostTop;
  FLblTitle.Margins.Bottom := 5;
  FLblTitle.Margins.Left := 4;
  FLblTitle.Margins.Right := 4;
  FLblTitle.Margins.Top := 5;
  FLblTitle.AutoSize := True;
  FLblTitle.Font.Style := [];
  FLblTitle.TextSettings.Font.Family := FFontName;
  FLblTitle.TextSettings.Font.Size := 16;
  FLblTitle.TextSettings.WordWrap := True;
  FLblTitle.TextSettings.FontColor := StringToAlphaColor(FTextColor);
  // FLblTitle.TextSettings.HorzAlign := TTextAlign.Center;

  { var LRect := TRectangle.Create(FLblTitle);
    LRect.Parent := FLblTitle;
    LRect.Align := TAlignLayout.Contents; }

  if not(Assigned(FLytButton)) then
    FLytButton := TLayout.Create(LytBackground);
  FLytButton.Name := 'LytButton' + Name;
  FLytButton.Align := TAlignLayout.Bottom;
  FLytButton.Parent := LytBackground;
  FLytButton.Margins.Bottom := 5;
  FLytButton.Margins.Left := 40;
  FLytButton.Margins.Right := 40;
  FLytButton.Margins.Top := 10;
  FLytButton.Height := 50;
  FLytButton.BringToFront;

  case MsgTypeButton of
    mtbOk:
      begin
        ConfigButton(OkText, TAlignLayout.HorzCenter, idOK);
      end;
    mtbYesNo:
      begin
        ConfigButton(YesText, TAlignLayout.Left, idYes);
        ConfigButton(NoText, TAlignLayout.Right, idNo);
      end;
    mtbLoading:
      raise Exception.Create('mtbLoading not implemented');
    mtbNone:
      FLytButton.Visible := False;
  end;
end;

procedure TFmxMessageDialog.Resize;
var
  LLenght: Single;
  LParentWidth: Single;
begin
  ExtraHeight :=
    FLytBackground.Padding.Bottom +
    FLytBackground.Padding.Top
    ;

  if FLytButton.Visible then
  begin
    ExtraHeight := ExtraHeight +
      FLytButton.Margins.Bottom +
      FLytButton.Margins.Top +
      FLytButton.Height;
  end;

  if FLblTitle.Visible then
  begin
    FLblTitle.RecalcSize;

    ExtraHeight := ExtraHeight +
      FLblTitle.Height +
      FLblTitle.Margins.Bottom +
      FLblTitle.Margins.Top;
  end;

  if FLenght <> -1 then
  begin
    LLenght := FLenght;

    if (FParent.Parent is TForm) then
      LParentWidth := TForm(FParent.Parent).ClientWidth
    else
      LParentWidth := TControl(FParent.Parent).Width;

    LParentWidth := LParentWidth - LytBackground.Margins.Left - LytBackground.Margins.Right;

    if LLenght > LParentWidth then
      LytBackground.Align := TAlignLayout.VertCenter
    else
    begin
      LytBackground.Align := TAlignLayout.Center;
      LytBackground.Width := LLenght;
    end;
  end;

  inherited;

end;

procedure TFmxMessageDialog.Show;
begin
  FLblTitle.Text := FTitle;

  FLblTitle.Visible := not FLblTitle.Text.Trim.IsEmpty;

  inherited;

end;

procedure TFmxMessageDialog.Tap(Sender: TObject; const Point: TPointF);
begin
  Click(Sender);
end;

initialization

TFmxMessage.OnInitialization;
TFmxMessageDialog.OnInitialization;

finalization

TFmxMessage.OnFinalization;
TFmxMessageDialog.OnFinalization;

end.
