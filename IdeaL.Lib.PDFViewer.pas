unit IdeaL.Lib.PDFViewer;

interface

uses
  System.IOUtils,
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,
  System.DateUtils,

  IdURI,

  {FMX.Forms,
    FMX.WebBrowser,
    FMX.Types,
    FMX.StdCtrls,
    FMX.Dialogs}
  Data.DB,

{$IFDEF ANDROID}
  Androidapi.JNI.GraphicsContentViewText,
  Androidapi.JNI.JavaTypes, // Salvar documento Stream
  Androidapi.JNI.Net, // Abrir arquivo
  Androidapi.Helpers, // StringToJString
  FMX.Helpers.Android,
  {$IF CompilerVersion >= 35.0}Androidapi.JNI.Support,{$ELSE}DW.Androidapi.JNI.FileProvider,{$ENDIF}
{$ENDIF }
{$IF DEFINED(IOS)}
  iOSApi.Foundation,
  Macapi.Helpers,
  FMX.Helpers.iOS,
{$ENDIF}
{$IFDEF MACOS}
  Posix.Stdlib,
{$ENDIF MACOS}
{$IFDEF MSWINDOWS}
  Winapi.ShellAPI, Winapi.Windows,
{$ENDIF MSWINDOWS}
  FMX.Objects,
  FMX.Surfaces;

type
  TPDFViewer = class
  private
    {$IFDEF ANDROID}
      class procedure OpenApi26Less(const AFilePath: string);
      class procedure OpenApi26More(const AFilePath: string);
    {$ENDIF}
    class procedure Open(const AFilePath: string);
    { private declarations }
  protected
    { protected declarations }
  public
    class procedure OpenPdf(const AFilePath: string);
    { public declarations }
  published
    { published declarations }
  end;

implementation

uses
IdeaL.Lib.Utils;

{ TPDFViewer }

{$IFDEF ANDROID}

class procedure TPDFViewer.OpenApi26Less(const AFilePath: string);
var
  Intent         : JIntent;
  Filepath       : String;
  SharedFilePath : string;
  tmpFile        : String;
begin
  {if not AExternalURL then
  begin
    Filepath       := TPath.Combine(TPath.GetDocumentsPath      , APDFFileName);
    SharedFilePath := TPath.Combine(TPath.GetSharedDocumentsPath, APDFFileName);

    if TFile.Exists(SharedFilePath) then
      TFile.Delete(SharedFilePath);

    TFile.Copy(Filepath, SharedFilePath);
  end;}

  Intent := TJIntent.Create;
  Intent.setAction(TJIntent.JavaClass.ACTION_VIEW);

  tmpFile := StringReplace(AFilePath, ' ', '%20', [rfReplaceAll]);


  {if AExternalURL then
//    Intent := TJIntent.JavaClass.init(TJIntent.JavaClass.ACTION_VIEW,
//      TJnet_Uri.JavaClass.parse(StringToJString(TIdURI.URLEncode(tmpFile))))
    Intent.setData(StrToJURI(tmpFile))
  else}
  Intent.setDataAndType(StrToJURI('file://' + AFilePath), StringToJString('application/pdf'));

  SharedActivity.startActivity(Intent);
end;

class procedure TPDFViewer.OpenApi26More(const AFilePath: string);
var
  LIntent: JIntent;
  LUri: Jnet_Uri;
  LJFile: Jfile;
  LStrFileProvider: string;
  LPath: string;
  LName: string;
begin
  LStrFileProvider := JStringToString
      (TAndroidHelper.Context.getApplicationContext.getPackageName) +
      '.fileprovider';

  LPath := System.IOUtils.TPath.GetDirectoryName(AFilePath);
  LName := System.IOUtils.TPath.GetFileName(AFilePath);

  LJFile := TJfile.JavaClass.init(
    Androidapi.Helpers.StringToJString(LPath),
    Androidapi.Helpers.StringToJString(LName)
    );

  LUri := {$IF CompilerVersion >= 35.0}TJcontent_FileProvider{$ELSE}TJFileProvider{$ENDIF}.JavaClass.getUriForFile(
    TAndroidHelper.Context,
    Androidapi.Helpers.StringToJString(LStrFileProvider),
    TJFile.JavaClass.init(StringToJString(AFilePath))
    );

  LIntent := TJIntent.JavaClass.init(TJIntent.JavaClass.ACTION_VIEW);
  LIntent.setDataAndType(LUri, StringToJString('application/pdf'));
  LIntent.setFlags(TJIntent.JavaClass.FLAG_GRANT_READ_URI_PERMISSION);
  TAndroidHelper.Activity.startActivity(LIntent);
end;

class procedure TPDFViewer.Open(const AFilePath: string);
begin
  if (IdeaL.Lib.Utils.TUtils.GetOsVersionInt >= 8) then
    OpenApi26More(AFilePath)
  else
    OpenApi26Less(AFilePath);
end;
{$ENDIF }
{$IF DEFINED(IOS)}

type
  TCloseParentFormHelper = class
  public
    procedure OnClickClose(Sender: TObject);
  end;

procedure TCloseParentFormHelper.OnClickClose(Sender: TObject);
begin
  TForm(TComponent(Sender).Owner).Close();
end;

class procedure TPDFViewer.Open(const AFilePath: string);
var
  NSU                      : NSUrl;
  OK                       : Boolean;
  frm                      : TForm;
  WebBrowser               : TWebBrowser;
  btn                      : TButton;
  btnShare                 : TButton;
  toolSuperior             : TToolBar;
  Evnt                     : TCloseParentFormHelper;
  tmpFile                  : String;
begin
  Frm                      := TForm.CreateNew(nil);

  toolSuperior             := TToolBar.Create(frm);
  toolSuperior.Align       := TAlignLayout.Top;
  toolSuperior.StyleLookup := 'toolbarstyle';
  toolSuperior.Parent      := frm;

  {Bot�o Back}
  btn                      := TButton.Create(frm);
  btn.Align                := TAlignLayout.Left;
  btn.Margins.Left         := 8;
  btn.StyleLookup          := 'backtoolbutton';
  btn.Text                 := 'Voltar';
  btn.Parent               := toolSuperior;

  WebBrowser               := TWebBrowser.Create(frm);
  WebBrowser.Parent        := frm;
  WebBrowser.Align         := TAlignLayout.Client;

  evnt                     := TCloseParentFormHelper.Create;
  btn.OnClick              := evnt.OnClickClose;

  if AExternalURL then
  begin
    tmpFile := StringReplace(APDFFileName, ' ', '%20', [rfReplaceAll]);
    WebBrowser.Navigate('http://' + tmpFile);
  end
  else
    WebBrowser.Navigate('file://' + TPath.Combine(TPath.GetDocumentsPath, APDFFileName));

  frm.ShowModal();

end;
{$ENDIF}
{$IFDEF MACOS}

class procedure TPDFViewer.Open(const AFilePath: string);
begin
  _system(PAnsiChar('open '+'"'+AnsiString(AFilePath)+'"'));
end;
{$ENDIF MACOS}
{$IFDEF MSWINDOWS}

class procedure TPDFViewer.Open(const AFilePath: string);
begin
  ShellExecute(0, 'OPEN', PChar(AFilePath), '', '', SW_SHOWNORMAL);
end;
{$ENDIF MSWINDOWS}

class procedure TPDFViewer.OpenPdf(const AFilePath: string);
begin
  TPDFViewer.Open(AFilePath);
end;

end.
