unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Ani, FMX.Layouts;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Layout1: TLayout;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    procedure ShowMessage(const AResult: TModalResult);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  IdeaL.Lib.Message;

{$R *.fmx}

procedure TForm1.Button1Click(Sender: TObject);
begin
  TFmxMessage.Toast('test test test test test ', 'Arial');
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  TFmxMessage.Dialog(
    'Which option do you want?' + sLineBreak +
    'Test' + sLineBreak +
    'Test' + sLineBreak +
    'Test' + sLineBreak +
    'Test' + sLineBreak +
    'Test' + sLineBreak +
    'Test' + sLineBreak +
    'Test' + sLineBreak +
    'Test' + sLineBreak +
    'Test' + sLineBreak +
    '',
    'Message Question',
    'Black',
    'Arial',
    'White',
    EmptyStr, // Here you must send the Button.CustomStely you created
    mtbYesNo,
    mtInformation,
    ShowMessage,
    nil,
    600);
end;

procedure TForm1.ShowMessage(const AResult: TModalResult);
begin
  case AResult of
    idYes:
      TFmxMessage.Dialog(
        'You pressed yes.' + sLineBreak + 'What will you choose now?',
        'Message Question',
        'Black',
        'Arial',
        'White',
        EmptyStr,  // Here you must send the Button.CustomStely you created
        mtbYesNo,
        mtQuestion,
        ShowMessage,
        nil,
        600);
    idNo:
      TFmxMessage.Dialog(
        'You pressed No',
        'Message Question',
        'Black',
        'Arial',
        'White',
        EmptyStr,  // Here you must send the Button.CustomStely you created
        mtbOk,
        mtInformation,
        nil,
        nil,
        600);
  end;
end;

end.
