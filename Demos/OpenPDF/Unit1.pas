unit Unit1;

{
  Remind, any configuration must be done for 32 and 64 bits, preferably All Configurations

  Needed permissions on Android
  Read and Write External Storages

  Project-Options-Application-Entitlement List-Secure File Sharing = True

  File provider (some of these links might help you):
  (take a look at the Manifest.Template on D11 example folder)
  https://stackoverflow.com/questions/56598480/couldnt-find-meta-data-for-provider-with-authority
  https://stackoverflow.com/questions/54510186/delphi-use-android-fileprovider-to-send-intent-to-open-and-image-file-with-the-d?noredirect=1&lq=1
  https://imperiumdelphi.wordpress.com/2019/03/04/o-tal-do-fileprovider-do-android/
  https://www.delphipraxis.net/200744-fehler-beim-android-fileprovider-datei-wird-nicht-geoeffnet.html
  https://blogs.embarcadero.com/opening-a-pdf-on-android-with-delphi/
}

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    procedure OpenPdf;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  System.IOUtils,
  IdeaL.Lib.PDFViewer,
  IdeaL.Lib.Android.Permissions;

{$R *.fmx}

procedure TForm1.Button1Click(Sender: TObject);
begin
{$IFDEF ANDROID}
  TPermissions.GetPermissions(
    [ptReadExternalStorage, ptWriteExternalStorage],
    OpenPdf,
    procedure
    begin
      ShowMessage('The permissions are needed')
    end
    );
{$ELSE}
   OpenPdf;
{$ENDIF}
end;

procedure TForm1.OpenPdf;
var
  LPath: string;
begin
  LPath := System.IOUtils.TPath.GetDocumentsPath;
  LPath := System.IOUtils.TPath.Combine(LPath, 'pdf-test.pdf');
  if not (FileExists(LPath)) then
    raise Exception.Create('File [' + LPath + ' does not exist');

  TPDFViewer.OpenPdf(LPath);
end;

end.
