unit FormMain;

// See IdeaL.Lib.PushNotification for all comments

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Memo.Types,
  FMX.StdCtrls, FMX.ScrollBox, FMX.Memo, FMX.Controls.Presentation;

type
  TForm2 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    memoDeviceToken: TMemo;
    memoDeviceId: TMemo;
    memoMessage: TMemo;
    procedure FormCreate(Sender: TObject);
  private
    procedure DoReceivingNotification(AMessage: string);
    procedure DoDeviceTokenHasBeenTaken;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses
  IdeaL.Lib.PushNotification;

{$R *.fmx}

{ TForm2 }

procedure TForm2.DoDeviceTokenHasBeenTaken;
begin
  memoDeviceToken.Lines.Add(TPushNotificationReceiver.Instance.DeviceToken);
end;

procedure TForm2.DoReceivingNotification(AMessage: string);
begin
  memoMessage.Lines.Add(AMessage);
end;

procedure TForm2.FormCreate(Sender: TObject);
var
  LArray: TArray<string>;
begin
  memoDeviceId.Lines.Add(TPushNotificationReceiver.Instance.DeviceId);
  TPushNotificationReceiver.Instance.OnReceiveNotificationEvent := DoReceivingNotification;
  TPushNotificationReceiver.Instance.OnDeviceTokenHasBeenTaken := DoDeviceTokenHasBeenTaken;
  LArray := TPushNotificationReceiver.Instance.StartupNotifications;
  for var i := 0 to Pred(Length(LArray)) do
    memoMessage.Lines.Add(LArray[i]);
end;

end.
