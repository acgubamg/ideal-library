program Server;

uses
  System.StartUpCopy,
  FMX.Forms,
  FormMain in 'FormMain.pas' {Form1},
  IdeaL.Lib.PushNotification in '..\..\..\IdeaL.Lib.PushNotification.pas';

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := True;

  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
