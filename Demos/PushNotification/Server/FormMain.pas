unit FormMain;

// See IdeaL.Lib.PushNotification for all comments

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Memo.Types,
  FMX.ScrollBox, FMX.Memo, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit,
  FMX.Layouts;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Button1: TButton;
    memoMessage: TMemo;
    memoLog: TMemo;
    CheckBox1: TCheckBox;
    edtToken: TEdit;
    edtNotificationId: TEdit;
    Label4: TLabel;
    edtNotificationTitle: TEdit;
    Label5: TLabel;
    edtNotificationBody: TEdit;
    Label6: TLabel;
    Layout1: TLayout;
    Layout2: TLayout;
    procedure Button1Click(Sender: TObject);
  private
    procedure WriteMemoLog(Value: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  IdeaL.Lib.PushNotification;

{$R *.fmx}


procedure TForm1.Button1Click(Sender: TObject);
var
  LTitle: string;
  LId: string;
  LBody: string;
begin
  if (CheckBox1.IsChecked) and
    ((edtNotificationId.Text.Trim.IsEmpty) or
    (edtNotificationTitle.Text.Trim.IsEmpty) or
    (edtNotificationBody.Text.Trim.IsEmpty))
  then
    raise Exception.Create('Notification info has to be filled out');

  TPushNotificationSender.Instance.SenderId := 'SENDERID';
  TPushNotificationSender.Instance.ApiKey := 'APIKEY';

  LTitle := '';
  LId := '';
  LBody := '';

  if CheckBox1.IsChecked then
  begin // Sending that inforation will force the notification to appear
    LTitle := edtNotificationTitle.Text;
    LId := edtNotificationId.Text;
    LBody := edtNotificationBody.Text;
  end;

  memoLog.Lines.Clear;

  TPushNotificationSender.Instance.Send(
    LTitle,
    LId,
    LBody,
    [edtToken.Text],
    ['message'],
    [memoMessage.Lines.Text],
    WriteMemoLog,
    WriteMemoLog
    );
end;

procedure TForm1.WriteMemoLog(Value: string);
begin
  memoLog.Lines.Add(Value);
end;

end.
