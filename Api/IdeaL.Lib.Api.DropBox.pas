unit IdeaL.Lib.Api.DropBox;

{ ***
  https://www.dropbox.com/developers/documentation/http/documentation
  *** }

interface

uses
  System.Classes,
  System.SysUtils,
  System.IOUtils,

  System.Net.URLClient,
  System.Net.HttpClient,
  System.Net.HttpClientComponent;

const
  cUrlApiFiles = 'https://api.dropboxapi.com/2/files';
  cUrlContentFiles = 'https://content.dropboxapi.com/2/files';

  // Api
  cEventCreateFolder = 'create_folder_v2';
  cEventDelete = 'delete';
  cEventListFolder = 'list_folder';
  // Content
  cEventDownload = 'download';
  cEventUpload = 'upload';

type
  TDownloadStatusUpdate = reference to procedure(const ALength, ACount: Int64);

  TDropBoxApi = class
  private
    FNetHttpRequest: TNetHTTPRequest;
    FNetHttpClient: TNetHTTPClient;
    FAccessToken: string;
    FDownloadStatusUpdate: TDownloadStatusUpdate;
    FRequestCompletedEvent: TRequestCompletedEvent;

    function GetNetHttpRequest(const AContentType: string = '';
      const AMethodString: string = ''): TNetHTTPRequest;

    function GetUrlCreateFolder(): string;
    function GetUrlDelete(): string;
    function GetUrlDownload(): string;
    function GetUrlListFolder(): string;
    function GetUrlUpload(): string;

    function GetAccessToken(): string;

    // Library
    procedure ValidateResponse(AResponse: IHTTPResponse);
    function IfThenString(const AValue: Boolean; const AMin, AMax: string): string; overload;
    function IfThenString(const AValue: Boolean): string; overload;
    { private declarations }
  protected
    procedure DestroyObjs;
    procedure NetHttpReceiveDataEvent(const Sender: TObject; AContentLength: Int64; AReadCount: Int64; var Abort: Boolean);
    procedure NetHttpRequestCompletedEvent(const Sender: TObject; const AResponse: IHTTPResponse);
    { protected declarations }
  public
    class var DropBoxAccessToken: string;

    constructor Create();
    destructor Destroy; override;

    procedure CreateFolder(
      const AServerForlderFullPath: string;
      const AAutoRename: Boolean = False
      );
    procedure Delete( // Delete file or folder
      const AServerForlderFullPath: string
      );
    procedure Download(
      const ALocalFileFullName: string;
      const AServerFileFullName: string
      );
    function ListFolder(
      const AServerForlderFullPath: string;
      const ARecursive: Boolean = False;
      const AIncludeMediaInfo: Boolean = False;
      const AIncludeDeleted: Boolean = False;
      const AIncludeHasExplicitSharedMembers: Boolean = False;
      const AIncludeMountedFolders: Boolean = True;
      const AIncludeNonDownloadableFiles: Boolean = True
      ): string;
    procedure Upload(
      const ALocalFileFullName: string;
      const AServerFileFullName: string
      );
    { public declarations }
  published
    property AccessToken: string read GetAccessToken write FAccessToken;

    property OnDownloadStatusUpdate: TDownloadStatusUpdate read FDownloadStatusUpdate write FDownloadStatusUpdate;
    property OnRequestCompletedEvent: TRequestCompletedEvent read FRequestCompletedEvent write FRequestCompletedEvent;
    { published declarations }
  end;

implementation

{ TDropBox }

constructor TDropBoxApi.Create();
begin
  inherited;
  FAccessToken := EmptyStr;
  FDownloadStatusUpdate := nil;
  FRequestCompletedEvent := nil;
  FNetHttpRequest := nil;
  FNetHttpClient := nil;
end;

procedure TDropBoxApi.CreateFolder(const AServerForlderFullPath: string;
  const AAutoRename: Boolean);
var
  LJson: string;
  LAutoRename: string;
  LResponse: IHTTPResponse;
  JsonToSend: TStringStream;
begin
  try
    LAutoRename := 'false';
    GetNetHttpRequest('application/json', 'POST');
    if AAutoRename then
      LAutoRename := 'true';

    LJson :=
      '{' +
      '"path": "' + AServerForlderFullPath + '"' +
      ', "autorename": ' + IfThenString(AAutoRename) +
      '}'
      ;

    JsonToSend := TStringStream.Create(UTF8Encode(LJson));

    ValidateResponse(
      FNetHttpRequest.Post(
      GetUrlCreateFolder,
      JsonToSend
      ));
  finally
    FreeAndNil(JsonToSend);
  end;
end;

procedure TDropBoxApi.Delete(const AServerForlderFullPath: string);
var
  LJson: string;
  LResponse: IHTTPResponse;
  JsonToSend: TStringStream;
begin
  try
    GetNetHttpRequest('application/json', 'POST');

    LJson :=
      '{' +
      '"path": "' + AServerForlderFullPath + '"' +
      '}'
      ;

    JsonToSend := TStringStream.Create(UTF8Encode(LJson));

    ValidateResponse(
      FNetHttpRequest.Post(
      GetUrlDelete,
      JsonToSend
      ));
  finally
    FreeAndNil(JsonToSend);
  end;
end;

destructor TDropBoxApi.Destroy;
begin
  DestroyObjs;
  inherited;
end;

procedure TDropBoxApi.DestroyObjs;
begin
  if (Assigned(FNetHttpClient)) then
    FreeAndNil(FNetHttpClient);
  if (Assigned(FNetHttpRequest)) then
    FreeAndNil(FNetHttpRequest);
end;

procedure TDropBoxApi.Download(const ALocalFileFullName,
  AServerFileFullName: string);
var
  LStrResp: TMemoryStream;
  LResponse: IHTTPResponse;
begin
  try
    GetNetHttpRequest;
    LStrResp := TMemoryStream.Create;
    FNetHttpRequest.CustomHeaders['Dropbox-API-Arg'] := Format('{"path": "%s"}', [AServerFileFullName]);
    LResponse := FNetHttpRequest.Get(GetUrlDownload, LStrResp);
    ValidateResponse(LResponse);
    LStrResp.Position := 0;

    TThread.Synchronize(nil,
      procedure()
      begin
        LStrResp.SaveToFile(ALocalFileFullName);

        if not FileExists(ALocalFileFullName) then
         raise Exception.Create(
          'TDropBoxApi.Download couldn''t save the file : ' + ALocalFileFullName + sLineBreak +
          'Check if you have the right permissions'
          );
      end);

  finally
    FreeAndNil(LStrResp);
  end;
end;

function TDropBoxApi.GetAccessToken: string;
begin
  Result := FAccessToken;
  if (Result.Trim.IsEmpty) then
    Result := DropBoxAccessToken;
end;

function TDropBoxApi.GetNetHttpRequest(const AContentType,
  AMethodString: string): TNetHTTPRequest;
begin
  DestroyObjs;

  FNetHttpClient := TNetHTTPClient.Create(nil);
  FNetHttpClient.Accept := '*/*';
  FNetHttpClient.AcceptCharSet := 'utf-8';
  FNetHttpClient.AcceptEncoding := 'gzip, deflate, br';
  FNetHttpClient.ContentType := AContentType;
  FNetHttpClient.UserAgent :=
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Acoo Browser; ' +
    'GTB5; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; ' +
    'Maxthon; InfoPath.1; .NET CLR 3.5.30729; .NET CLR 3.0.30618)';

  FNetHttpRequest := TNetHTTPRequest.Create(nil);
  FNetHttpRequest.OnReceiveData := NetHttpReceiveDataEvent;
  FNetHttpRequest.OnRequestCompleted := NetHttpRequestCompletedEvent;
  FNetHttpRequest.Client := FNetHttpClient;
  FNetHttpRequest.CustomHeaders['Authorization'] := 'Bearer ' + AccessToken;
  FNetHttpRequest.Accept := '*/*';
  FNetHttpRequest.AcceptCharSet := 'utf-8';
  FNetHttpRequest.AcceptEncoding := 'gzip, deflate, br';
  FNetHttpRequest.MethodString := AMethodString;

  Result := FNetHttpRequest;
end;

function TDropBoxApi.GetUrlCreateFolder: string;
begin
  Result := cUrlApiFiles + '/' + cEventCreateFolder;
end;

function TDropBoxApi.GetUrlDelete: string;
begin
  Result := cUrlApiFiles + '/' + cEventDelete;
end;

function TDropBoxApi.GetUrlDownload: string;
begin
  Result := cUrlContentFiles + '/' + cEventDownload;
end;

function TDropBoxApi.GetUrlListFolder: string;
begin
  Result := cUrlApiFiles + '/' + cEventListFolder;
end;

function TDropBoxApi.GetUrlUpload: string;
begin
  Result := cUrlContentFiles + '/' + cEventUpload;
end;

function TDropBoxApi.IfThenString(const AValue: Boolean): string;
begin
  Result := IfThenString(AValue, 'true', 'false');
end;

function TDropBoxApi.IfThenString(const AValue: Boolean;
const AMin, AMax: string): string;
begin
  if AValue then
    Result := AMin
  else
    Result := AMax;
end;

function TDropBoxApi.ListFolder(const AServerForlderFullPath: string;
const ARecursive, AIncludeMediaInfo, AIncludeDeleted,
  AIncludeHasExplicitSharedMembers, AIncludeMountedFolders,
  AIncludeNonDownloadableFiles: Boolean): string;
var
  LJson: string;
  JsonToSend: TStringStream;
  LResponse: IHTTPResponse;
begin
  try
    Result := EmptyStr;
    GetNetHttpRequest('application/json', 'POST');

    LJson :=
      '{' +
      '"path": "' + AServerForlderFullPath + '"' +
      ',"recursive": ' + IfThenString(ARecursive) +
      ',"include_media_info": ' + IfThenString(AIncludeMediaInfo) +
      ',"include_deleted": ' + IfThenString(AIncludeDeleted) +
      ',"include_has_explicit_shared_members": ' +
      IfThenString(AIncludeHasExplicitSharedMembers) +
      ',"include_mounted_folders": ' + IfThenString(AIncludeMountedFolders) +
      ',"include_non_downloadable_files": ' +
      IfThenString(AIncludeNonDownloadableFiles) +
      '}'
      ;

    JsonToSend := TStringStream.Create(UTF8Encode(LJson));

    LResponse :=
      FNetHttpRequest.Post(
      GetUrlListFolder,
      JsonToSend
      );
    ValidateResponse(LResponse);
    Result := LResponse.ContentAsString();
  finally
    FreeAndNil(JsonToSend);
  end;
end;

procedure TDropBoxApi.NetHttpReceiveDataEvent(const Sender: TObject;
AContentLength, AReadCount: Int64; var Abort: Boolean);
begin
  if (Assigned(FDownloadStatusUpdate)) then
    FDownloadStatusUpdate(AContentLength, AReadCount);
end;

procedure TDropBoxApi.NetHttpRequestCompletedEvent(const Sender: TObject;
const AResponse: IHTTPResponse);
begin
  if (Assigned(FRequestCompletedEvent)) then
    FRequestCompletedEvent(Sender, AResponse);
end;

procedure TDropBoxApi.Upload(const ALocalFileFullName,
  AServerFileFullName: string);
var
  LStrResp: TFileStream;
  LResponse: IHTTPResponse;
begin
  try
    LStrResp :=
      TFileStream.Create(ALocalFileFullName, fmOpenRead or fmShareDenyWrite);

    GetNetHttpRequest();
    FNetHttpRequest.CustomHeaders['Dropbox-API-Arg'] :=
      Format('{"path": "%s"}', [AServerFileFullName]);
    FNetHttpRequest.Client.ContentType := 'application/octet-stream';
    LResponse := FNetHttpRequest.Post(GetUrlUpload, LStrResp);
    ValidateResponse(LResponse);
  finally
    FreeAndNil(LStrResp);
  end;
end;

procedure TDropBoxApi.ValidateResponse(AResponse: IHTTPResponse);
begin
  If (AResponse.StatusCode <> 200) Then
    raise Exception.Create(
      'Error: ' + AResponse.StatusCode.ToString + ' ' + AResponse.StatusText);
end;

end.
