unit IdeaL.Lib.Loading;

interface

uses
  System.Classes,
  System.UITypes,
  System.UIConsts,
  System.SysUtils,

  FMX.Controls,
  FMX.Objects,
  FMX.Graphics,
  FMX.Layouts,
  FMX.Types,
  FMX.StdCtrls,
  FMX.Effects,
  FMX.Dialogs,
  FMX.Forms,
  FMX.Filter.Effects;

type
  TLoading = class
  private
  class var
    FLytBackground: TLayout;
    FRctBackground, FRctLoading: TRectangle;
    FSdwMessage: TShadowEffect;
    FLblMessage: TLabel;
    FAniIndicator: TAniIndicator;
    AFillRGBEffect: TFillRGBEffect;

    FStyleBook: TStyleBook;
    FParent: TControl;
    FLenght: Single;

    class var FAniColor: string;
    class var FBackgroundColor: string;
    class var FLblMessageColor: string;
    class var FLblMessageFontName: string;

    class procedure PrepareBackground(const aParent: TControl);
    class procedure FRctBackgroundClick(Sender: TObject);
    class procedure Resize();
  public
    class procedure Show(const AMsg, AFontName, AMessageColor, ABackgroundColor, AAniColor: String; const ALenght: Single = -1);
    class procedure ChangeMessage(const AMsg: String);
    class procedure Hide();
  end;

implementation

{ TMessage }

class procedure TLoading.ChangeMessage(const AMsg: String);
begin
  TThread.Synchronize(nil,
  procedure()
  begin
    if Assigned(FLblMessage) then
      FLblMessage.Text := AMsg;
    Resize();
  end);
end;

class procedure TLoading.FRctBackgroundClick(Sender: TObject);
begin
{$IFDEF DEBUG}
  //Hide();
{$ENDIF}
end;

class procedure TLoading.Hide;
begin
{$REGION 'Free Components'}
  if (AFillRGBEffect <> nil) then
  begin
    AFillRGBEffect.DisposeOf;
    AFillRGBEffect := nil;
  end;

  if (FAniIndicator <> nil) then
  begin
    FAniIndicator.DisposeOf;
    FAniIndicator := nil;
  end;

  if (FLblMessage <> nil) then
  begin
    FLblMessage.DisposeOf;
    FLblMessage := nil;
  end;

  if (FSdwMessage <> nil) then
  begin
    FSdwMessage.DisposeOf;
    FSdwMessage := nil;
  end;

  if (FRctLoading <> nil) then
  begin
    FRctLoading.DisposeOf;
    FRctLoading := nil;
  end;

  if (FRctBackground <> nil) then
  begin
    FRctBackground.DisposeOf;
    FRctBackground := nil;
  end;

  if (FLytBackground <> nil) then
  begin
    FLytBackground.SendToBack;
    FLytBackground.Parent := Nil;
    FLytBackground.DisposeOf;
    FLytBackground := nil;
  end;
{$ENDREGION}
  if (Assigned(FParent)) then
    FParent.SetFocus;
end;

class procedure TLoading.PrepareBackground(const aParent: TControl);
var
  LText: string;
  LMargins: Single;
  LLenghtMainForm: Single;
begin
  FParent := aParent;
  LText := Application.MainForm.Name;
  FLytBackground := TLayout.Create(Application.MainForm);
  //FLytBackground.Visible := False;
  FLytBackground.Position.Y := 0;
  FLytBackground.Position.X := 0;
  FLytBackground.Width := Application.MainForm.ClientWidth;
  FLytBackground.Height := Application.MainForm.ClientHeight;

{$REGION 'FRctBackground'}
  LText := Application.MainForm.Name;
  FRctBackground := TRectangle.Create(Application.MainForm);
  FRctBackground.Stroke.Kind := TBrushKind.None;
  FRctBackground.Fill.Color := StringToAlphaColor('Black');
  FRctBackground.Align := TAlignLayout.Contents;
  FRctBackground.Parent := FLytBackground;
  FRctBackground.OnClick := FRctBackgroundClick;
{$ENDREGION}
//
{$REGION 'FRctLoading'}
  LMargins := 20;

  if(FLenght <> -1)then
  begin
    LLenghtMainForm := Application.MainForm.ClientWidth;
    LLenghtMainForm := LLenghtMainForm - FLenght;
    LMargins := Trunc(LLenghtMainForm/2);
  end;

  LText := Application.MainForm.Name;
  FRctLoading := TRectangle.Create(Application.MainForm);
  FRctLoading.Stroke.Kind := TBrushKind.None;
  FRctLoading.Fill.Color := StringToAlphaColor(FBackgroundColor);
  FRctLoading.Align := TAlignLayout.VertCenter;
  FRctLoading.Parent := FLytBackground;
  FRctLoading.Margins.Left := LMargins;
  FRctLoading.Margins.Right := LMargins;
  FRctLoading.XRadius := 8;
  FRctLoading.yRadius := 8;
  FRctLoading.BringToFront;
  FRctLoading.Height := 100;

  FSdwMessage := TShadowEffect.Create(Application.MainForm);
  FSdwMessage.Parent := FRctLoading;
{$ENDREGION}
//
{$REGION 'FAniIndicator'}
  FAniIndicator := TAniIndicator.Create(Application.MainForm);
  FAniIndicator.Enabled := False;
  FAniIndicator.Parent := FRctLoading;
  FAniIndicator.Align := TAlignLayout.MostLeft;
  FAniIndicator.Margins.Left := 10;
  FAniIndicator.Margins.Right := 5;
  FAniIndicator.Width := 50;
{$ENDREGION}
//
{$REGION 'FAniIndicator'}
  AFillRGBEffect := TFillRGBEffect.Create(Application.MainForm);
  AFillRGBEffect.Parent := FAniIndicator;
  AFillRGBEffect.Enabled := True;
  AFillRGBEffect.Color := StringToAlphaColor(FAniColor);
  // );
{$ENDREGION}
//
{$REGION 'FLblMessage'}
  FLblMessage := TLabel.Create(Application.MainForm);
  FLblMessage.Parent := FRctLoading;
  FLblMessage.Align := TAlignLayout.Client;
  FLblMessage.Margins.Bottom := 5;
  FLblMessage.Margins.Left := 5;
  FLblMessage.Margins.Right := 5;
  FLblMessage.Margins.Top := 5;
  FLblMessage.StyledSettings := [];
  FLblMessage.AutoSize := True;
  FLblMessage.Font.Style := [];
  FLblMessage.TextSettings.Font.Family := FLblMessageFontName;
  FLblMessage.TextSettings.Font.Size := 16;
  FLblMessage.TextSettings.FontColor := StringToAlphaColor(FLblMessageColor);
  // );
  FLblMessage.TextSettings.HorzAlign := TTextAlign.Center;
{$ENDREGION}
//
  FLytBackground.Visible := True;
  FRctBackground.Visible := True;

  FLytBackground.Opacity := 0;
  FRctBackground.Opacity := 0;

  FLytBackground.Parent := Application.MainForm;
  FLytBackground.Align := TAlignLayout.Contents;

  FRctBackground.AnimateFloat('opacity', 0.3);
  {$IFDEF MSWINDOWS}
  FLytBackground.AnimateFloatWait('opacity', 1);
  {$ELSE}
  FLytBackground.AnimateFloat('opacity', 1);
  {$ENDIF}

  FAniIndicator.Enabled := True;

  FLytBackground.BringToFront;
  FAniIndicator.BringToFront;
  FLytBackground.SetFocus;
end;

class procedure TLoading.Resize();
begin
  FRctLoading.Height := FLblMessage.Height + FLblMessage.Margins.Bottom +
    FLblMessage.Margins.Top;

  // para nao permitir que ele suma com os botoes
  if (FRctLoading.Height > FLytBackground.Height) then
    FRctLoading.Height := FLytBackground.Height - 20
  else if (FRctLoading.Height < 40) then
    FRctLoading.Height := 40;
end;

class procedure TLoading.Show(const AMsg, AFontName, AMessageColor,
  ABackgroundColor, AAniColor: String; const ALenght: Single);
begin
  FAniColor := AAniColor;
  FBackgroundColor := ABackgroundColor;
  FLblMessageColor := AMessageColor;
  FLblMessageFontName := AFontName;
  FLenght := ALenght;

  PrepareBackground(TControl(Application.MainForm));
  FLblMessage.Text := AMsg;
  Resize();
end;

end.
