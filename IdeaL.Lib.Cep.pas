unit IdeaL.Lib.Cep;

interface

uses
  System.SysUtils,
  System.StrUtils,
  System.Classes,

  Data.DB,

  System.Net.URLClient,
  System.Net.HttpClient,
  System.Net.HttpClientComponent,

  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Param,
  FireDAC.Stan.Error,
  FireDAC.DatS,
  FireDAC.Phys.Intf,
  FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  FireDAC.Stan.StorageJSON;

type
  TCep = class
  private
    class function CreateMemCep(): TFDMemTable;
    class function GetCepViaCep(const aCep: String): TFDMemTable;
    class function GetViaCep(const aCep: string): string;
    class procedure MemCepBeforePost(DataSet: TDataSet);
    { private declarations }
  public
    class procedure GetCep(const aCep: string;
      var aResultStatus: TMemoryStream);

    class function GetJson(const aCep: string): string;
    { public declarations }
  end;

implementation

{ TCep }

class function TCep.CreateMemCep: TFDMemTable;
begin
  Result := TFDMemTable.Create(nil);
  Result.FieldDefs.Add('ResultCod', ftString, 8); { OK ERRO VAZIO }
  Result.FieldDefs.Add('ResultMessage', ftString, 200); { Message de Erro }
  Result.FieldDefs.Add('Cep', ftString, 8);
  Result.FieldDefs.Add('UfCod', ftString, 10);
  Result.FieldDefs.Add('UfDesc', ftString, 20);
  Result.FieldDefs.Add('UfAbreviacao', ftString, 2);
  Result.FieldDefs.Add('CidadeCod', ftString, 10);
  Result.FieldDefs.Add('CidadeDesc', ftString, 100);
  Result.FieldDefs.Add('Logradouro', ftString, 100);
  Result.FieldDefs.Add('Bairro', ftString, 50);
  Result.FieldDefs.Add('Complemento', ftString, 200);

  Result.BeforePost := MemCepBeforePost;
end;

class procedure TCep.GetCep(const aCep: string;
  var aResultStatus: TMemoryStream);
var
  vMessageValidar: String;

  vTryStrToInt: integer;

  vMemCep: TFDMemTable;
begin
  try
{$REGION 'Valida CEP'}
    vMessageValidar := '';

    if not(TryStrToInt(aCep, vTryStrToInt)) then
      vMessageValidar := 'CEP deve possuir apenas n�meros.';

    if (aCep.Length <> 8) then
      vMessageValidar := 'CEP deve possuir 8 n�meros.';

    if (vMessageValidar <> '') then
    begin
      vMemCep := CreateMemCep();
      vMemCep.CreateDataSet;
      vMemCep.Append;
      vMemCep.FieldByName('ResultCod').AsString := 'ERRO';
      vMemCep.FieldByName('ResultMessage').AsString := vMessageValidar;
      vMemCep.Post;

      vMemCep.SaveToStream(aResultStatus, FireDAC.Stan.Intf.sfJSON);

      Exit;
    end
    else
{$ENDREGION}
    //
    begin
      vMemCep := GetCepViaCep(aCep);

      if (vMemCep.FieldByName('ResultCod').AsString <> 'OK') then
      begin
        // Faco as outras pesquisas
      end;

      vMemCep.SaveToStream(aResultStatus, FireDAC.Stan.Intf.sfJSON);
    end;
  finally
    try
      FreeAndNil(vMemCep);
    except
    end;
  end;
end;

class function TCep.GetCepViaCep(const aCep: String): TFDMemTable;

{$REGION 'StrTran'}
  function StrTran(sString, sOld, sNew: String): String;
  var
    i, iPos: integer;
    sResult: String;
  begin
    sResult := sString;
    iPos := 1;
    for i := 1 to Length(sString) do
    begin
      if Copy(sResult, iPos, Length(sOld)) = sOld then
      begin
        Delete(sResult, iPos, Length(sOld));
        Insert(sNew, sResult, iPos);
        iPos := iPos + (Length(sNew));
      end
      else
        iPos := iPos + 1;
    end;
    Result := sResult;
  end;
{$ENDREGION}
{$REGION 'TrataCep'}
  function TrataCep(cJson, cVar: string): string;
  var
    cCEP: string;
  begin
    cCEP := Copy(cJson, Pos(cVar, cJson), 50);
    cCEP := Copy(cCEP, Pos('":', cCEP) + 2, 50);
    cCEP := Copy(cCEP, 0, Pos('",', cCEP));
    cCEP := StrTran(cCEP, '"', '');
    Result := Trim(cCEP);
  end;
{$ENDREGION}

var
  vNetHttpRequest: TNetHTTPRequest;
  vNetHttpClient: TNetHTTPClient;

  vJson: string;

  vMemCep: TFDMemTable;
begin
  try
    vNetHttpClient := TNetHTTPClient.Create(nil);
    vNetHttpRequest := TNetHTTPRequest.Create(nil);
    vNetHttpRequest.Client := vNetHttpClient;

    vMemCep := CreateMemCep();

    try
      vJson := vNetHttpRequest.Get('http://viacep.com.br/ws/' + aCep + '/json/')
        .ContentAsString(TEncoding.UTF8);

      vMemCep.CreateDataSet;
      vMemCep.Append;
      if (TrataCep(vJson, 'cep') <> '') then
      begin
        vMemCep.FieldByName('ResultCod').AsString := 'OK';
        vMemCep.FieldByName('Cep').AsString := aCep;
        vMemCep.FieldByName('CidadeCod').AsString := TrataCep(vJson, 'ibge');
        vMemCep.FieldByName('CidadeDesc').AsString :=
          TrataCep(vJson, 'localidade');
        vMemCep.FieldByName('Logradouro').AsString :=
          TrataCep(vJson, 'logradouro');
        vMemCep.FieldByName('Bairro').AsString := TrataCep(vJson, 'bairro');
        vMemCep.FieldByName('Complemento').AsString :=
          TrataCep(vJson, 'complemento');
        vMemCep.FieldByName('UfAbreviacao').AsString := TrataCep(vJson, 'uf');
      end
      else
      begin
        vMemCep.FieldByName('ResultCod').AsString := 'VAZIO';
      end;
      vMemCep.Post;
    except
      on E: Exception do
      begin
        vMemCep.Cancel;
        vMemCep.Append;
        vMemCep.FieldByName('ResultCod').AsString := 'ERRO';
        vMemCep.FieldByName('ResultMessage').AsString := E.Message;
        vMemCep.Post;
      end;
    end;

    Result := vMemCep;
  finally
    try
      FreeAndNil(vNetHttpClient);
    except
    end;
    try
      FreeAndNil(vNetHttpRequest);
    except
    end;
    // try FreeAndNil(vMemCep); except end;
  end;
end;

class function TCep.GetJson(const aCep: string): string;
var
  LTryStrToInt: integer;
begin
  Result := EmptyStr;
  if not(TryStrToInt(aCep, LTryStrToInt)) then
    raise Exception.Create('CEP deve possuir apenas n�meros.');

  if (aCep.Length <> 8) then
    raise Exception.Create('CEP deve possuir 8 n�meros.');

  Result := GetViaCep(aCep);

  // if (Result.Trim.IsEmpty) then
  // Consultar em outros WS

  if (Result.Trim.IsEmpty) then
    raise Exception.Create('CEP [' + ACep + '] n�o localizado');
end;

class function TCep.GetViaCep(const aCep: string): string;
var
  LNetHttpRequest: TNetHTTPRequest;
  LNetHttpClient: TNetHTTPClient;

  vMemCep: TFDMemTable;
begin
  try
    LNetHttpClient := TNetHTTPClient.Create(nil);
    LNetHttpRequest := TNetHTTPRequest.Create(nil);
    LNetHttpRequest.Client := LNetHttpClient;

    Result := LNetHttpRequest.Get('http://viacep.com.br/ws/' + aCep + '/json/')
      .ContentAsString(TEncoding.UTF8);

    if (Result.Trim.IsEmpty) then
      Result := EmptyStr
    else
    begin
      Result := StringReplace(Result, '"localidade":', '"cidade":',
        [rfReplaceAll]);
      Result := StringReplace(Result, '"ibge":', '"cidadeibge":',
        [rfReplaceAll]);
      Result := '[' + Result + ']';
    end;
  finally
    FreeAndNil(LNetHttpRequest);
    FreeAndNil(LNetHttpClient);
  end;

end;

class procedure TCep.MemCepBeforePost(DataSet: TDataSet);
begin
  case AnsiIndexStr(DataSet.FieldByName('UfAbreviacao').AsString,
    ['EX', 'RO', 'AC', 'AM', 'RR', 'PA', 'AP', 'TO', 'MA', 'PI', 'CE', 'RN',
    'PB', 'PE', 'AL', 'SE', 'BA', 'MG', 'ES', 'RJ', 'SP', 'PR', 'SC', 'RS',
    'MS', 'MT', 'GO', 'DF']) of
    0:
      DataSet.FieldByName('UfCod').AsString := '0';
    1:
      DataSet.FieldByName('UfCod').AsString := '11';
    2:
      DataSet.FieldByName('UfCod').AsString := '12';
    3:
      DataSet.FieldByName('UfCod').AsString := '13';
    4:
      DataSet.FieldByName('UfCod').AsString := '14';
    5:
      DataSet.FieldByName('UfCod').AsString := '15';
    6:
      DataSet.FieldByName('UfCod').AsString := '16';
    7:
      DataSet.FieldByName('UfCod').AsString := '17';
    8:
      DataSet.FieldByName('UfCod').AsString := '21';
    9:
      DataSet.FieldByName('UfCod').AsString := '22';
    10:
      DataSet.FieldByName('UfCod').AsString := '23';
    11:
      DataSet.FieldByName('UfCod').AsString := '24';
    12:
      DataSet.FieldByName('UfCod').AsString := '25';
    13:
      DataSet.FieldByName('UfCod').AsString := '26';
    14:
      DataSet.FieldByName('UfCod').AsString := '27';
    15:
      DataSet.FieldByName('UfCod').AsString := '28';
    16:
      DataSet.FieldByName('UfCod').AsString := '29';
    17:
      DataSet.FieldByName('UfCod').AsString := '31';
    18:
      DataSet.FieldByName('UfCod').AsString := '32';
    19:
      DataSet.FieldByName('UfCod').AsString := '33';
    20:
      DataSet.FieldByName('UfCod').AsString := '35';
    21:
      DataSet.FieldByName('UfCod').AsString := '41';
    22:
      DataSet.FieldByName('UfCod').AsString := '42';
    23:
      DataSet.FieldByName('UfCod').AsString := '43';
    24:
      DataSet.FieldByName('UfCod').AsString := '50';
    25:
      DataSet.FieldByName('UfCod').AsString := '51';
    26:
      DataSet.FieldByName('UfCod').AsString := '52';
    27:
      DataSet.FieldByName('UfCod').AsString := '53';
  end;
end;

end.
